mod engine {
	pub mod parser;
	pub mod game_state;
	pub mod macros;

	pub mod interactibles;
	pub mod room;
}

extern crate rand;
extern crate lazy_static;
extern crate maplit;

use std::collections::HashMap;

use engine::game_state::{GameState, GameVars};
use engine::room::Room;
use engine::interactibles::*;
use engine::room::Direction;

use maplit::hashmap;


fn main() {
	let mut int_dragon = Dragon::new();
	let mut int_chest = Chest::new();
	let mut int_corpse = Corpse::new();

	let room_cave = Room::new(
		s!("Cave"),
		s!("You are in a cave."),
		hashmap!{
			Direction::North => s!("Hall"),
		},
		vec![],
		|gamevars| {"Nothing.".into()}
	);

	let room_hall = Room::new(
		s!("Hall"),
		s!("You are in a hall."),
		hashmap!{
			Direction::South => s!("Cave"),
			Direction::East => s!("Pit"),
		},
		vec![&mut int_dragon],
		|gamevars| {"There is a dragon here.".into()}
	);

	let room_pit = Room::new(
		s!("Pit"),
		s!("You are in a pit."),
		hashmap!{
			Direction::West => s!("Hall"),
			Direction::North => s!("Lake"),
		},
		vec![&mut int_chest],
		|gamevars| {"There is a chest here.".into()}
	);

	let room_lake = Room::new(
		s!("Lake"),
		s!("You are in a lake."),
		hashmap!{
			Direction::South => s!("Pit")
		},
		vec![&mut int_corpse],
		|gamevars| {"There is a corpse here.".into()}
	);

	let mut global_interacts = HashMap::<String, fn(&mut GameVars) -> String>::new();
	global_interacts.insert("stare blankly".into(), |gamevars| { "You stare blankly.".to_string() });

	let mut gamestate = GameState::new(
		hashmap!{
			s!("Cave") => room_cave,
			s!("Hall") => room_hall,
			s!("Pit") => room_pit,
			s!("Lake") => room_lake,
		},
		s!("Cave"),
		hashmap!{},
		global_interacts
	);

	println!("{}This is a test {}", con_green!(bold), con_reset!());
	
	gamestate.play();
}
