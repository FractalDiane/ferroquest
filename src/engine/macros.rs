// Macros

#[macro_export]
macro_rules! s {
	($str:literal) => {
		$str.to_string()
	};
}

#[macro_export]
macro_rules! kw_look {
	() => {
		"look" | "examine" | "check" | "inspect"
	};
}

#[macro_export]
macro_rules! kw_take {
	() => {
		"take" | "get" | "pick" | "pickup"
	};

	($str:literal) => {
		concat!("take", $str) | concat!("get", $str) | concat!("pick", $str) | concat!("pickup", $str)
	}
}

#[macro_export]
macro_rules! kw_north {
	() => {
		"north" | "n"
	};
}

#[macro_export]
macro_rules! kw_south {
	() => {
		"south" | "s"
	};
}

#[macro_export]
macro_rules! kw_west {
	() => {
		"west" | "w"
	};
}

#[macro_export]
macro_rules! kw_east {
	() => {
		"east" | "e"
	};
}

#[macro_export]
macro_rules! kw_quit {
	() => {
		"quit" | "exit"
	};
}

#[macro_export]
macro_rules! kw_inventory {
	() => {
		"inventory" | "inv"
	};
}

// Console colors

#[macro_export]
macro_rules! con_red {
	() => {
		"\x1b[0;31m"
	};

	(bold) => {
		"\x1b[1;31m"
	};

	(italic) => {
		"\x1b[3;31m"
	}
}

#[macro_export]
macro_rules! con_yellow {
	() => {
		"\x1b[1;33m"
	};

	(bold) => {
		"\x1b[1;33m"
	};

	(italic) => {
		"\x1b[3;33m"
	};
}

#[macro_export]
macro_rules! con_green {
	() => {
		"\x1b[0;32m"
	};

	(bold) => {
		"\x1b[1;32m"
	};

	(italic) => {
		"\x1b[3;32m"
	};
}

#[macro_export]
macro_rules! con_reset {
	() => {
		"\x1b[0m"
	};

	(bold) => {
		"\x1b[1m"
	};

	(italic) => {
		"\x1b[3m"
	};
}
