// Room

use crate::engine::interactibles::Interactible;
use crate::engine::game_state::GameVars;
use crate::s;

use std::collections::HashMap;
use maplit::hashmap;

#[derive(PartialEq, Eq, Hash)]
pub enum Direction {
	North,
	South,
	East,
	West,
}

pub struct Room<'a> {
	pub name: String,
	pub description: String,
	pub visited: bool,
	pub look_function: fn(&mut GameVars) -> String,

	interactibles: HashMap<String, Box<&'a mut dyn Interactible>>,
	pub adjacent_rooms: HashMap<Direction, String>,
}

impl<'a> Room<'a> {
	pub fn new(name: String, description: String, adjacent_rooms: HashMap<Direction, String>, interactibles: Vec<&'a mut dyn Interactible>, look_function: fn(&mut GameVars) -> String) -> Self {
		let mut room = Room{
			name,
			description,
			visited: false,
			look_function,

			interactibles: hashmap!{},
			adjacent_rooms,
		};

		for int in interactibles {
			room.interactibles.insert(int.name(), Box::new(int));
		}

		room
	}

	/*
	pub fn add_interactible(&'a mut self, interactible: &'a mut dyn Interactible) -> &'a mut Self {
		self.interactibles.insert(interactible.name(), Box::new(interactible));
		self
	}
	*/

	pub fn interact_with(&mut self, gamevars: &mut GameVars, target: String, keyword: String, args: Vec<String>) -> String {
		match self.interactibles.get_mut(&target.to_lowercase()) {
			Some(t) => {
				match t.interact(gamevars, &keyword, &args) {
					Ok(res) => res,
					Err(err) => err,
				}
			}

			None => {
				for int in self.interactibles.iter_mut() {
					let this = int.1;
					for aux in this.aux_targets() {
						match this.interact_aux(gamevars, &aux, &keyword, &args) {
							Ok(res) => {
								return res;
							}
							Err(_) => {}
						}
					}
				}

				s!("Cannot do.")
			}
		}
	}
}
