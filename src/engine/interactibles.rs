// Interactibles

use crate::s;
use crate::{kw_look, kw_take};
use crate::engine::game_state::GameVars;

pub trait Interactible {
	fn name(&self) -> String;
	fn aux_targets(&self) -> Vec<String> {
		vec![]
	}

	fn interact(&mut self, gamevars: &mut GameVars, keyword: &String, args: &Vec<String>) -> Result<String, String>;

	#[allow(unused)]
	fn interact_aux(&mut self, gamevars: &mut GameVars, aux_target: &String, keyword: &String, args: &Vec<String>) -> Result<String, String> {
		Err(s!("Cannot do."))
	}
}

// =================================================================================

pub struct Chest {
	opened: bool,
	has_sword: bool,
}

impl Chest {
	pub fn new() -> Self {
		Chest{opened: false, has_sword: true}
	}
}

impl Interactible for Chest {
	fn name(&self) -> String {
		s!("chest")
	}

	fn aux_targets(&self) -> Vec<String> {
		vec![s!("sword")]
	}

	fn interact(&mut self, gamevars: &mut GameVars, keyword: &String, _args: &Vec<String>) -> Result<String, String> {
		match keyword.as_str() {
			kw_look!() => {
				if self.opened {
					if self.has_sword {
						Ok(s!("There's a sword in it."))
					}
					else {
						Ok(s!("It's empty."))
					}
				}
				else {
					Ok(s!("It's closed."))
				}
			}

			"open" => {
				if gamevars.is_flag_set(&s!("key")) {
					self.opened = true;
					Ok(s!("You open the chest."))
				}
				else {
					Ok(s!("It's locked."))
				}
				
			}

			_ => {
				Err(s!("Cannot do."))
			}
		}
	}

	fn interact_aux(&mut self, gamevars: &mut GameVars, aux_target: &String, keyword: &String, _args: &Vec<String>) -> Result<String, String> {
		if *aux_target == s!("sword") {
			match keyword.as_str() {
				kw_take!() => {
					if self.opened {
						if self.has_sword {
							self.has_sword = false;
							gamevars.flags.insert(s!("sword"), 1);
							Ok(s!("Taken."))
						}
						else {
							Err(s!("Cannot do."))
						}
					}
					else {
						Err(s!("Cannot do."))
					}
				}
				_ => {
					Err(s!("Cannot do."))
				}
			}
		}
		else {
			Err(s!("Cannot do."))
		}
	}
}

// =================================================================================

pub struct Dragon {
	//alive: bool,
}

impl Dragon {
	pub fn new() -> Self {
		Dragon{/*alive: true*/}
	}
}

impl Interactible for Dragon {
	fn name(&self) -> String {
		s!("dragon")
	}

	fn interact(&mut self, gamevars: &mut GameVars, keyword: &String, _args: &Vec<String>) -> Result<String, String> {
		match keyword.as_str() {
			kw_look!() => {
				Ok(s!("He looks angry."))
			}

			"kill" => {
				*gamevars.quit = true;
				if gamevars.is_flag_set(&s!("sword")) {
					Ok(s!("You won."))
				}
				else {
					Ok(s!("You died."))
				}
			}

			_ => {
				Err(s!("Cannot do."))
			}
		}
	}
}

// =================================================================================

pub struct Corpse {
	has_key: bool,
}

impl Corpse {
	pub fn new() -> Self {
		Corpse{has_key: true}
	}
}

impl Interactible for Corpse {
	fn name(&self) -> String {
		s!("corpse")
	}

	fn interact(&mut self, _gamevars: &mut GameVars, keyword: &String, _args: &Vec<String>) -> Result<String, String> {
		match keyword.as_str() {
			kw_look!() => {
				//gamevars.flags.insert(s!("saw_key"), 1);
				if self.has_key { Ok(s!("There is a key on the corpse.")) } else { Ok(s!("Nothing noteworthy.")) }
			}

			_ => {
				Err(s!("Cannot do."))
			}
		}
	}

	fn aux_targets(&self) -> Vec<String> {
		vec![s!("key")]
	}

	fn interact_aux(&mut self, gamevars: &mut GameVars, aux_target: &String, keyword: &String, _args: &Vec<String>) -> Result<String, String> {
		if aux_target == "key" {
			match keyword.as_str() {
				kw_take!() => {
					if self.has_key {
						self.has_key = false;
						gamevars.flags.insert(s!("key"), 1);
						Ok(s!("Taken."))
					}
					else {
						Err(s!("Cannot do."))
					}
				}

				_ => {
					Err(s!("Cannot do."))
				}
			}
		}
		else {
			Err(s!("Cannot do."))
		}
	}
}
