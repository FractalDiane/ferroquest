// GameState

use crate::engine::room::{Room, Direction};
use crate::engine::parser;
use std::io::{Write, stdin, stdout};
use std::collections::{HashMap, HashSet};
use crate::{kw_north, kw_south, kw_west, kw_east, kw_quit, kw_inventory, kw_look};

use maplit::hashset;

pub struct GameVars<'a> {
	pub flags: &'a mut HashMap<String, i32>,
	pub inventory: &'a mut HashSet<String>,
	pub quit: &'a mut bool,
}

impl<'a> GameVars<'a> {
	pub fn is_flag_set(&self, flag: &String) -> bool {
		match self.flags.get(flag) {
			Some(f) => *f > 0,
			None => false
		}
	}
}

pub struct GameState<'a> {
	flags: HashMap<String, i32>,
	inventory: HashSet<String>,

	map: HashMap<String, Room<'a>>,
	current_room_name: String,

	global_interacts: HashMap<String, fn(&mut GameVars) -> String>,
}

impl<'a> GameState<'a> {
	pub fn new(map: HashMap<String, Room<'a>>, starting_room: String, flags: HashMap<String, i32>, global_interacts: HashMap<String, fn(&mut GameVars) -> String>) -> Self {
		GameState{
			map,
			current_room_name: starting_room,
			flags,
			global_interacts,
			inventory: hashset!{}
		}
	}

	pub fn play(&mut self) {
		let mut quit = false;
		while !quit {
			let mut gamevars = GameVars{flags: &mut self.flags, inventory: &mut self.inventory, quit: &mut quit};
			let current_room = self.map.get_mut(&self.current_room_name).unwrap();
			current_room.visited = true;
			println!("\n{}", current_room.description);

			print!("> ");
			stdout().flush().unwrap();
			let mut input = String::new();
			stdin().read_line(&mut input).unwrap();

			let keyword = parser::get_keyword(&input);
			let args = parser::get_args(&input);
			let keyword_str = keyword.as_str();
			match keyword_str {
				kw_quit!() => {
					*gamevars.quit = true;
					println!("LATER!");
					continue;
				},

				kw_inventory!() => {
					let inv = &self.inventory;
					for item in inv.into_iter() {
						println!("{}", item);
					}

					continue;
				},

				kw_look!() => {
					if args.is_empty() {
						println!("{}", (current_room.look_function)(&mut gamevars));
						continue;
					}
				}

				kw_north!() | kw_south!() | kw_west!() | kw_east!() => {
					let dir = match keyword_str {
						kw_north!() => Direction::North,
						kw_south!() => Direction::South,
						kw_west!() => Direction::West,
						kw_east!() | _ => Direction::East,
					};

					match current_room.adjacent_rooms.get(&dir) {
						Some(room) => {
							self.current_room_name = room.clone();
						},
						None => {
							println!("Cannot do.");
						},
					}

					continue;
				},

				_ => {}
			}

			match self.global_interacts.get(input.trim_end()) {
				Some(int) => {
					println!("{}", (int)(&mut gamevars));
					continue;
				}

				None => {}
			}

			let target = parser::get_target(&input);

			let result = current_room.interact_with(&mut gamevars, target, keyword, args);
			println!("{}", result);
		}
	}

	// ============================================================================================
}
