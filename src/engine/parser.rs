// Parser

pub fn get_keyword(string: &String) -> String {
	string.split_whitespace().collect::<Vec<&str>>()[0].to_string().to_lowercase()
}


pub fn get_args(string: &String) -> Vec<String> {
	let args = string.split_whitespace().collect::<Vec<&str>>()[1..].to_vec();
	let mut args_str = Vec::<String>::new();
	for string in args {
		args_str.push(string.to_string().to_lowercase());
	}

	args_str
}


pub fn get_target(string: &String) -> String {
	string.split_whitespace().collect::<Vec<&str>>().pop().unwrap().to_string().to_lowercase()
}
